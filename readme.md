# Csv2Sqlite

## Purpose

This is a simple tool for importing CSV file into a SQlite database.

## Usage
```
csv2sqlite.py [-h] -c CSV -f SQLITE_FILE [--sql-pre SQL_PRE] [--sql-post SQL_POST] [-q QUIET]
optional arguments:
  -h, --help            show this help message and exit
  -c CSV, --csv CSV     Source CSV file
  -f SQLITE_FILE, --sqlite-file SQLITE_FILE
                        Output SQLite file
  --sql-pre SQL_PRE     Mask of SQL files to apply before import
  --sql-post SQL_POST   Mask of SQL files to apply after import
  -q QUIET, --quiet QUIET
                        No output at all
```

## Example
```
csv2sqlite.py -c source.csv -f target.sqlite --sql-pre table.sql
```

The above example imports data from source.csv into the target.sqlite file.
Before the procedure, table.sql will be executed (possibly creating a table
in target.sqlite).

## Tips

You can use `--sql-post` to create indeces.
