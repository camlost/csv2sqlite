#!/usr/bin/env python

##
# csv2sqlite.py

CSV_DELIMITER = ';'
CSV_QUOTECHAR = '"'

DB_TABLE = "data"

import argparse
import csv
import glob
import sqlite3

def loadCsv2Sqlite(db, csv_file, sql_pre, sql_post, delim, quote_char):
	query_mask = "INSERT INTO {}".format(DB_TABLE) + r" VALUES({})"
	with open(csv_file) as fin:
		db = sqlite3.connect(db)
		applySQlFiles(db, sql_pre)

		rcsv = csv.reader(fin, delimiter=delim, quotechar=quote_char)
		col_count = 0
		query = ""
		idx = 0
		for row in rcsv:
			if idx == 0:
				# Get number of columns
				col_count = len(row)
				sql_args = ','.join('?' for i in range(col_count))
				query = query_mask.format(sql_args)

			db.execute(query, row)
			idx += 1

		applySQlFiles(db, sql_post)
		db.commit()
		db.close()

def applySQlFiles(db, mask):
	for item in glob.iglob(mask):
		with open(item) as f:
			sql_cmd = f.read()
			db.execute(sql_cmd)

def main():
	parser = argparse.ArgumentParser(description="csv2sqlite - Import CSV file to SQlite DB")
	parser.add_argument("-c", "--csv", required=True, help="Source CSV file")
	parser.add_argument("-f", "--sqlite-file", required=True, help="Output SQLite file")
	parser.add_argument("--sql-pre", help="Mask of SQL files to apply before import")
	parser.add_argument("--sql-post", help="Mask of SQL files to apply after import")
	parser.add_argument("-q", "--quote-char", default=CSV_QUOTECHAR, help="CSV quote char")
	parser.add_argument("-d", "--delimiter", default=CSV_DELIMITER, help="CSV delimiter")

	args = parser.parse_args()
	loadCsv2Sqlite(args.sqlite_file, args.csv, args.sql_pre, args.sql_post,
		delim=args.delimiter, quote_char=args.quote_char)


if __name__ == "__main__":
	main()
